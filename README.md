# Schneider FFTK Report Extractor Tool #

This is a tool I created to extract information from a collection of FFTK reports. It iterates through from the master folder to find all reports and extracts the relevant information.

*Usage: generate.py <FolderName>*