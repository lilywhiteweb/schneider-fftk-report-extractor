#!venv/bin/python3
import xlrd
import optparse
import os
import csv

def main():

	p = optparse.OptionParser(description='Schneider Test Report Correlation Tool',
                              prog='generate',
                              version='0.1',
                              usage="usage: %prog FolderName")

	(options, args) = p.parse_args()

	# Check if folder name is supplied as argument
	if len(args) > 0:
		folder_name = args[0]

		# Check if folder actually exists
		if os.path.isdir(folder_name):
			
			print('Working...')
			
			fftk_files = []

			# Traverse the directory to find the FFTK results files
			for root, dirs, files in os.walk(folder_name):
				for file in files:
					if file.endswith(".xls"):
						 fftk_files.append(os.path.join(root, file))

			# Open csv file to write results
			results_file = folder_name + '_breaker_data.csv'

			with open(results_file, 'w') as csvfile:
				breaker_data = csv.writer(csvfile, delimiter=',')
				# Setup csv headers
				breaker_data.writerow(['filename', 'board_reference', 'cubicle_reference', 'trip_unit_serial_no', 'breaker_serial_no'])

				# Check each file and retrieve relevant cells
				for file in fftk_files:
					with xlrd.open_workbook(file) as wb:
						sheet_names = wb.sheet_names()
						if 'Test Report' in sheet_names:
							sheet = wb.sheet_by_name('Test Report')
							
							# Breaker Serial Numeber is N16
							# Trip unit serial number is N19
							# Board reference is D11
							# Cubicle reference is L11
							# Cell A1 is row 0 col 0
							breaker_serial_no = sheet.cell_value(15, 13)
							trip_unit_serial_no = sheet.cell_value(18, 13)
							board_reference = sheet.cell_value(10, 3)
							cubicle_reference = sheet.cell_value(10, 11)

							# Write out to the csv file
							breaker_data.writerow([file, board_reference, cubicle_reference, trip_unit_serial_no, breaker_serial_no])

			print('All done')
		else:
			print('Folder {} does not exist'.format(folder_name))
	else:
		print('No Folder Name Supplied')
		p.print_help()

if __name__ == '__main__':
	main()